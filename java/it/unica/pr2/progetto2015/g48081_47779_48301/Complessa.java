package it.unica.pr2.progetto2015.g48081_47779_48301;

public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

	
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
    public Object execute(Object... args) {

		double r = (double) args[0];
		int n = (int) args[1];
		double m = (double) args[2];
		double f = (double) args[3];
		int t = (int) args[4];

		return (double) (-m*((Math.pow((1+r),n)-1)/r)*(1 + (r*t)) - f)/(Math.pow((1+r),n));
	}


    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
		return "Financial";
	}

    /** Informazioni di aiuto */
    public final String getHelp() {
		return "Restituisce il valore attuale derivante da una serie di pagamenti regolari";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName() {
		return "VA";
	}

}
