package it.unica.pr2.progetto2015.g48081_47779_48301;

public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

	
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
    public Object execute(Object... args) {		
		double prod = 1;

		for(Object va : args){
			prod *= (double) va;
		}

		return prod;			
	}


    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
		return "Matematica";
	}

    /** Informazioni di aiuto */
    public final String getHelp() {
		return "Esegue il prodotto di una lista di valori";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName() {
		return "PRODOTTO";
	}

}
